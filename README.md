# The Family

All public resources available about the secretive cult, The Family (also known as The Fellowship), led by Douglas Coe on a mission to secretively rule the world with his version of Jesus, at all costs.

This is meant to be a hub for journalists to share sources and discover more to piece things together for the benefit of the public. 

Merge Requests are encouraged. Looking for co-maintainers. 

## Films

The Family on Netflix - https://www.netflix.com/title/80063867 (start here)

## Books

https://en.wikipedia.org/wiki/The_Family:_The_Secret_Fundamentalism_at_the_Heart_of_American_Power

C Street


## Podcasts

https://soundcloud.com/user-5491958/elite-fundamentalism-and-right#t=0:01 (2019-09-10)

https://interfaithradio.org/Archive/2019-September/The_Family__Unmasking_the__Christian_Mafia_ (interview with author Jeff Sharlet) (2019-09-06)

https://www.podbean.com/media/share/pb-mn9z2-be21fb#.XW8uHQPERiI.twitter (2019-09-04)

https://slate.com/podcasts/trumpcast/2019/08/why-does-donald-trump-command-such-unwavering-support-from-christian-fundamentalists (2019-08-21)

https://www.npr.org/templates/story/story.php?storyId=120746516 (2009-11-24)

## Articles

## Social Feeds

https://twitter.com/hashtag/TheFamilyNetflix 


## Members

Douglas Coe  
Jeff Sessions  
Mike Pence  
add more please... 



